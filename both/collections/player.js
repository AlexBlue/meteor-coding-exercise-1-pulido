Player = new Mongo.Collection('player');

var schema = {};

schema.Player = new SimpleSchema ({
    
    firstName: {
        type:String
    },
    lastName: {
        type:String
    },
    email: {
        type:String
    }
});

Player.attachSchema (schema.Player);

/*
 * Add query methods like this:
 *  Player.findPublic = function () {
 *    return Player.find({is_public: true});
 *  }
 */